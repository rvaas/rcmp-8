# -*- coding: utf8 -*-

import itertools
import numpy as np
import pandas as pd
from os import path
from statsmodels.stats.multitest import multipletests

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'invasion_single.csv'), index_col=False)
df = df.dropna(axis=0,how='any')

# Modelling
import statsmodels.api as sm
import statsmodels.formula.api as smf
formula = 'Value ~ C(Day) + J774*DMOG'
model = smf.ols(formula, df)
fit = model.fit()
summary = fit.summary()
anova_summary = sm.stats.anova_lm(fit, typ=3)

# Write data
with open("invasion_single_modelGLM.txt", "w") as text_file:
    text_file.write(summary.as_text())
with open("invasion_single_modelANOVA.txt", "w") as text_file:
    text_file.write(anova_summary.to_string())

# Print output
print(anova_summary)
print(summary)
